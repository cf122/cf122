#include<stdio.h>
struct emp  
{     
	int Eid;  
	char Ename[20];  
	struct Date  
	{  
		int dd;  
		int mm;  
		int yyyy;   
	}d;
	float salary;
};  
int main()
{
	struct emp e;
	printf("Enter the id,name,date of joining in(dd/mm/yyy) and salary of the employee\n");
	scanf("%d%s%d%d%d%f",&e.Eid,e.Ename,&e.d.dd,&e.d.mm,&e.d.yyyy,&e.salary);
	printf("Employee id : %d\n", e.Eid);  
	printf("Employee name : %s\n", e.Ename);  
	printf("Employee date of joining (dd/mm/yyyy) : %d/%d/%d\n", e.d.dd,e.d.mm,e.d.yyyy);  
	printf("Employee salary : %f\n",e.salary);
	return 0;  
}   
